class CreatePawns < ActiveRecord::Migration
  def change
    create_table :pawns do |t|
      t.string :color
      t.integer :user_id
      t.integer :game_id
      t.timestamps null: false
    end
  end
end
