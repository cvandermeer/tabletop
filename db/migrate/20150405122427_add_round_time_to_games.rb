class AddRoundTimeToGames < ActiveRecord::Migration
  def change
    add_column :games, :join_deadline, :datetime
  end
end
