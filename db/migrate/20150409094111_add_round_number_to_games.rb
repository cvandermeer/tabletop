class AddRoundNumberToGames < ActiveRecord::Migration
  def change
    add_column :games, :round_number, :integer
  end
end
