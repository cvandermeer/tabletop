class AddNextRoundToGames < ActiveRecord::Migration
  def change
    add_column :games, :next_round, :datetime
  end
end
