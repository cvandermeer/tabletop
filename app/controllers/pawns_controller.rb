class PawnsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_pawn, only: [:show]
  before_action :set_game, only: [:new, :create]

  def show
  end

  def new
    @pawn = Pawn.new
  end

  def create
    @pawn = Pawn.new(pawn_params)
    @pawn.user = current_user
    @pawn.game = @game
    if @pawn.save
      redirect_to @game
    else
      redirect_to @game
    end
  end

  private
    def pawn_params
      params.require(:pawn).permit(:color)
    end

    def set_pawn
      @pawn = Pawn.find(params[:id])
    end

    def set_game
      @game = Game.find(params[:game_id])
    end
end