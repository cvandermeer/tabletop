class GamesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_game, only: [:show]
  before_action :progress_games
  

  def index
    @games = Game.all
  end

  def show
  end

  def new
    @game = Game.new
  end

  def create
    @game = Game.new(game_params)
    if @game.save
      redirect_to @game
    else
      render :new
    end
  end

  private
    def progress_games
      Game.all.each do |game|
        if game.next_round < Time.now
          game.round_number= (game.round_number + 1)
        end 

        if game.round_number == 3
          redirect_to root_path, notice: 'Game afgelopen'
          game.destroy
        elsif game.next_round < Time.now
          game.next_round = (game.next_round + 60)
          game.save
        end
      end
    end

    def set_game
      @game = Game.find(params[:id])
    end

    def game_params
      params.require(:game).permit(:title)
    end
end