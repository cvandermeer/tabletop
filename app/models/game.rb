class Game < ActiveRecord::Base
  ### CALLBACKS ###
  before_create :set_default

  ### RELATIONS ###
  has_many :pawns, dependent: :destroy

  def set_default
    self.join_deadline =  (Time.now + 60)
    self.next_round = (Time.now + 60)
    self.round_number = 0
  end
end