class Pawn < ActiveRecord::Base
  ### RELATIONS ###
  belongs_to :user
  belongs_to :game

  ### VALIDATIONS ###
  validates :color, presence: true
  validates :user_id, presence: true
end